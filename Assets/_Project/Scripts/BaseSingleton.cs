﻿using UnityEngine;

/// <summary>
/// Inherit from this base class to create a singleton.
/// e.g. public class MyClassName : Singleton<MyClassName> {}
/// 
/// NOTE: This is my prefered way to create Singletons, and I've
/// been using ths method for a long time
/// Implementation courtsey of Unify Wiki: http://wiki.unity3d.com/index.php/Singleton
/// </summary>
public class BaseSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    // Check to see if we're about to be destroyed.
    private static bool ShuttingDown = false;
    private static object Lock = new object();
    private static T instance;

    public bool DontDestroy = false;

    /// <summary>
    /// Access singleton instance through this propriety.
    /// </summary>
    public static T Instance
    {
        get
        {
            if (ShuttingDown)
            {
                Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                    "' already destroyed. Returning null.");
                return null;
            }

            lock (Lock)
            {
                if (instance == null)
                {
                    // Search for existing instance.
                    instance = (T)FindObjectOfType(typeof(T));

                    // Create new instance if one doesn't already exist.
                    if (instance == null)
                    {
                        // Need to create a new GameObject to attach the singleton to.
                        var singletonObject = new GameObject();
                        instance = singletonObject.AddComponent<T>();
                        singletonObject.name = typeof(T).ToString() + " (Singleton)";

                        // Make instance persistent.
                        DontDestroyOnLoad(singletonObject);
                    }
                }

                return instance;
            }
        }
    }

    protected virtual void Awake()
    {
        if (DontDestroy)
            DontDestroyOnLoad(gameObject);
    }


    private void OnApplicationQuit()
    {
        ShuttingDown = true;
    }


    private void OnDestroy()
    {
        ShuttingDown = true;
    }

}