﻿using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// This class is responsible for listening to any screen tap which fires
/// off an event
/// NOTE: In this example, the Player Controller is listening for this event
/// to make the character jump
/// </summary>
public class ScreenTapListener : MonoBehaviour
{
    /// <summary>
    /// Touch variable for detecting touches.
    /// For use with Jumping
    /// </summary>
    public delegate void ScreenTapHandler();
    public static ScreenTapHandler OnScreenTapped;
    public static ScreenTapHandler OnScreenReleased;


    private void Update()
    {
        // We will wantt to send an event here in the future instead
        // TODO: Currently registers even over the Joystick, so we want to block the touch if we touch over UI
        if(Input.touchCount > 0)
        {
            Touch[] currentTouches = Input.touches;

            for(int i = 0; i < Input.touchCount; i++)
            {
                if (currentTouches[i].phase == TouchPhase.Began)
                {
                    Debug.Log("Touch detected!");

                    // Checking if the touch overlaps UI so the player doesn't jump when only moving the joystick
                    // Could be optimized as this uses a "Find" behind the scenes and should be found a different way
                    if (!EventSystem.current.IsPointerOverGameObject(currentTouches[i].fingerId))
                    {
                        OnScreenTapped?.Invoke();
                    }
                }

                else if (currentTouches[i].phase == TouchPhase.Ended)
                {
                    OnScreenReleased?.Invoke();
                }
            }





            /*
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Debug.Log("Touch detected!");

                // Checking if the touch overlaps UI so the player doesn't jump when only moving the joystick
                // Could be optimized as this uses a "Find" behind the scenes and should be found a different way
                if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                {
                    OnScreenTapped?.Invoke();
                }

            }
            */
        }
       
    }
}
